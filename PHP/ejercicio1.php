<!DOCTYPE HTML>
<html lang="ca">
<head>
<meta charset="utf-8">
<title>Ejercicio 1</title>
</head>
<body>
 <?php
  echo "<h1>Ejercicio 1</h1>";
  $varArray = array('Element1','Element2','Element3');
  echo "<p>" . $varArray[0] . "</p>";
  echo "<p>" . $varArray[1] . "</p>";
  echo "<p>" . $varArray[2] . "</p>";
  $varArray2 = array(
         'clau1'=>'Element4',
         'clau2'=>'Element5',
         'clau3'=>'Element6');
  echo "<p>" . $varArray2[clau1] . "</p>";
  echo "<p>" . $varArray2[clau2] . "</p>";
  echo "<p>" . $varArray2[clau3] . "</p>";
  echo "<p><pre>";
  print_r($varArray);
  echo "</pre></p>";
  echo "<p><pre>";
  print_r($varArray2);
  echo "</pre></p>";
  $varArray3 = array(
         'clau1'=>'9',
         'clau2'=>'Rojo',
         'clau3'=>'Azul');
  echo "<p><pre>";
  print_r($varArray3);
  echo "</pre></p>";
  echo "<p>Función array_keys</p>";
  print_r(array_keys($varArray3));
  echo "<p>Función array_keys para buscar 'Rojo':</p>";
  print_r(array_keys($varArray3,"Rojo"));
  echo "<p>Función array_reverse para invertir el array:</p>";
  print_r(array_reverse($varArray3));
 ?>
</body>
</html>

