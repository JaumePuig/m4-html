<!DOCTYPE html>
<html lang="ca">
 <head>
  <meta charset="utf-8">
  <title>Ejercicio 2</title>
 </head>
 <body>
  <?php
   $var1 = "5";
   $var2 = "9";
   echo "<h1>Ejercicio 2</h1>";
   echo "<h2>Funciones matemáticas</h2>";
   echo "<p>Raíz cuadrada de $var2:</p>";
   echo sqrt($var2);
   echo "<p>Valor más alto entre $var1 y $var2:</p>";
   echo max($var1,$var2);
   echo "<p>Valor aleatorio:</p>";
   echo rand();
   echo "<p>Convertir el número 9 decimal a binario:</p>";
   echo decbin(9);
  ?>
 </body>
</html>
